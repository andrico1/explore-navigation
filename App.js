import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';
import FIB from './components/Fib';

function App() {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>

        <FIB />

      </SafeAreaView>
    </>
  );
};

export default App;
