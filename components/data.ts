import { FAQ } from './RouteTypes';

export const faqData: FAQ[] = [
  {
    id: 'lump-sum',
    question: 'What is a lump sum?',
    answer:
      'The lump sum is a one time payment equating to the amount of total benefit that will be paid to your loved ones following a successful claim. This amount is calculated as your chosen percentage of your monthly salary multiplied by the remaining term (in months) left on the policy. ',
  },
  {
    id: 'how-lump-sum-calculated',
    question: 'How is the lump sum calculated?',
    answer: `This policy will pay out a lump sum to your loved ones if you die during the term of the policy. The amount that will be paid to your loved ones is calculated as follows:\n\n
Your chosen percentage multiplied by your monthly salary at the start of the policy multiplied by the remaining term (number of months) left on the policy. This amount reduces as the term of the policy reduces.\n\n
The amount that would be paid out to your loved ones decreases every month. Therefore the older you get, the lower the lump sum would be. The value of the lump sum reduces to zero at the end of the term of the policy. reduces to zero at the end of the term of the policy.`,
  },
  {
    id: 'salary-increase',
    question: 'What if my salary increases?',
    answer:
      'Your salary and percentage are chosen and fixed at the time you take out the policy. There is no automatic increase in your cover and premium as a result of inflation or a pay rise. However, you can increase your cover when certain life events happen, for example marriage, buying a mortgage etc. For more details, visit this section:',
  },
  {
    id: 'increase-cover',
    question: 'Can I increase my cover in future?',
    answer: `Yes, you have the option to increase your amount of cover without the need for further medical questions in the event of:

- Change in marital status
- Increase in mortgage
- Change in salary due to a new job or promotion
- Birth of your child or legal adoption

**The amount you can increase your cover by is limited to the following:**\n
*Whichever is lower:*\n
- £1050 x remaining term in months
- Percentage increase in monthly mortgage
- Percentage increase in your monthly income up to a maximum of 10%

**When you can’t change your cover amount:**

- If you are older than 55
- If you are diagnosed with or receiving medical treatment for a terminal illness`,
  },
];
