export type Route = 'PAGE_ONE' | 'PAGE_TWO' | 'PAGE_THREE' | 'FAQ_CONTAINER';

export const PAGE_ONE = 'PAGE_ONE';
export const PAGE_TWO = 'PAGE_TWO';
export const PAGE_THREE = 'PAGE_THREE';
export const FAQ_CONTAINER = 'FAQ_CONTAINER';

export interface FAQ {
  id: string;
  question: string;
  answer: string;
}
