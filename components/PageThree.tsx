import React from 'react';
import { View, Button, Text, FlatList } from 'react-native';
import { Navigation } from './history';
import { PAGE_ONE, PAGE_TWO, FAQ_CONTAINER } from './RouteTypes';
import { faqData } from './data';

export interface PageOneProps {
  navigation: Navigation;
  avatar: string;
  selectFaq: (faqId: string) => void;
}

function PageThree(props: PageOneProps) {
  const { navigation, selectFaq } = props;

  return (
    <View>
      <Button onPress={() => navigation.pop()} title="Back" />
      <Text>Browse your packages</Text>
      <Button onPress={() => navigation.push(PAGE_TWO)} title="Edit Salary" />
      <Button onPress={() => navigation.push(PAGE_ONE)} title="Next" />

      <FlatList
        data={faqData}
        renderItem={({ item }) => (
          <Text
            style={{ padding: 16 }}
            onPress={() => {
              selectFaq(item.id);
              return navigation.push(FAQ_CONTAINER);
            }}>
            {item.question}
          </Text>
        )}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

export default PageThree;
