import { useState } from 'react';
import { Route, PAGE_ONE } from './RouteTypes';

export interface Navigation {
  history: Route[];
  length: number;
  currentRoute: Route;
  push: (route: Route, passProps?: object) => void;
  pop: () => void;
  replace: (route: Route, passProps?: object) => void;
  navigationProps: any;
}

export function useNavigation(
  initialRoute: Route,
  defaultHistory: Route[] = [],
): Navigation {
  const [history, setHistory] = useState([...defaultHistory, initialRoute]);
  const [navigationProps, setNavigationProps] = useState({});

  const currentRoute = history.length ? history[history.length - 1] : PAGE_ONE;

  function push(route: Route, passProps: object = {}) {
    setNavigationProps(passProps);
    return setHistory([...history, route]);
  }

  function pop() {
    if (history.length === 0) {
      return null;
    }

    const historyClone = [...history];
    const prevRoute = historyClone.splice(historyClone.length - 1, 1)[0];

    setNavigationProps({});
    setHistory(historyClone);

    return prevRoute;
  }

  function replace(route: Route, passProps: object = {}) {
    pop();
    push(route, passProps);
  }

  return {
    history,
    length: history.length,
    currentRoute,
    navigationProps,
    push,
    pop,
    replace,
  };
}
