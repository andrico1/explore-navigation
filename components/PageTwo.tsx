import React from 'react';
import { View, Button, Text, TextInput } from 'react-native';
import { Navigation } from './history';
import { PAGE_THREE } from './RouteTypes';

export interface PageOneProps {
  navigation: Navigation;
}

function PageOne(props: PageOneProps) {
  const { navigation } = props;

  return (
    <View>
      <Button onPress={() => navigation.pop()} title="Back" />
      <Text>Edit Salary</Text>
      <TextInput value="123" onChange={() => null} />
      <Button onPress={() => navigation.push(PAGE_THREE)} title="Next" />
    </View>
  );
}

export default PageOne;
