import React from 'react';
import { View, Button, Text } from 'react-native';
import { Navigation } from './history';
import { PAGE_TWO } from './RouteTypes';

export interface PageOneProps {
  navigation: Navigation;
}

function PageOne(props: PageOneProps) {
  const { navigation } = props;

  return (
    <View>
      <Button onPress={() => navigation.pop()} title="Back" />
      <Text>Welcome to FIB</Text>
      <Button onPress={() => navigation.push(PAGE_TWO)} title="Next" />
    </View>
  );
}

export default PageOne;
