import React from 'react';
import { Navigation } from './history';
import { FAQ, FAQ_CONTAINER } from './RouteTypes';
import FaqScreen from './FaqScreen';
import { faqData } from './data';

interface IFibFaqContainer {
  faq: FAQ;
  navigation: Navigation;
  selectFaq: (faqId: string) => void;
}

interface NestedFaq {
  onPress: () => void;
  faq: FAQ;
}

function FaqContainer(props: IFibFaqContainer) {
  const { faq, navigation, selectFaq } = props;
  const { navigationProps } = navigation;

  function onNavigateBack() {
    navigation.pop();
    if (navigationProps.onNavigateBack) {
      navigationProps.onNavigateBack();
    }
  }

  if (faq.id === 'salary-increase') {
    const childFaqs = buildChildFaqs(navigation, selectFaq);

    return (
      <FaqScreen
        onNavigateBack={onNavigateBack}
        faq={faq}
        childFaqs={childFaqs}
      />
    );
  }

  return <FaqScreen onNavigateBack={onNavigateBack} faq={faq} />;
}

export default FaqContainer;

const buildChildFaqs = (
  navigation: Navigation,
  selectFaq: (faqId: string) => void,
): NestedFaq => {
  return {
    onPress: () => {
      navigation.push(FAQ_CONTAINER, {
        onNavigateBack: () => selectFaq('salary-increase'),
      });

      return selectFaq('increase-cover');
    },
    faq: faqData.find(({ id }) => id === 'increase-cover')!,
  };
};
