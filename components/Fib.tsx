import React, { useState } from 'react';
import { View } from 'react-native';
import { PageOne, PageTwo, PageThree } from '.';
import { useNavigation, Navigation } from './history';
import { Route, PAGE_ONE, PAGE_TWO, FAQ_CONTAINER } from './RouteTypes';
import FaqContainer from './FaqContainer';
import { faqData } from './data';

interface AllProps {
  navigation: Navigation;
  selectedFaq: string;
  selectFaq: (faqId: string) => void;
}

// how we handle modals

function getComponent(route: Route, allProps: AllProps) {
  if (route === PAGE_ONE) {
    return <PageOne navigation={allProps.navigation} />;
  }

  if (route === PAGE_TWO) {
    return <PageTwo navigation={allProps.navigation} />;
  }

  if (route === FAQ_CONTAINER) {
    const faq = faqData.find(({ id }) => id === allProps.selectedFaq);
    return (
      <FaqContainer
        faq={faq!}
        navigation={allProps.navigation}
        selectFaq={allProps.selectFaq}
      />
    );
  }

  return (
    <PageThree
      selectFaq={allProps.selectFaq}
      navigation={allProps.navigation}
      avatar="This is an avatar placeholder"
    />
  );
}

function FIBContainer() {
  const navigation = useNavigation(PAGE_ONE);
  const [selectedFaq, selectFaq] = useState<string>('');

  console.log(navigation.history);

  const component = getComponent(navigation.currentRoute, {
    navigation,
    selectedFaq,
    selectFaq,
  });

  return <View>{component}</View>;
}

export default FIBContainer;
