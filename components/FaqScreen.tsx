import React from 'react';
import { FAQ } from './RouteTypes';
import { View, Button, Text } from 'react-native';

interface Props {
  faq: FAQ;
  onNavigateBack: () => void;
  childFaqs?: {
    onPress: () => void;
    faq: FAQ;
  };
}

function FaqScreen(props: Props) {
  const { faq, onNavigateBack, childFaqs } = props;

  return (
    <View>
      <Button onPress={onNavigateBack} title="Back" />
      <View>
        <Text>{faq.question}</Text>
        <Text>{faq.answer}</Text>
        {!childFaqs ? null : (
          <Text style={{ padding: 16 }} onPress={childFaqs.onPress}>
            {childFaqs.faq.question}
          </Text>
        )}
      </View>
    </View>
  );
}

export default FaqScreen;
